﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace FogCreek
{
    [TestFixture]
    public class Solution
    {
        private const string Alphabet = "acdegilmnoprstuw";

        [Test]
        public void Hash_Should_Work()
        {
            var hash = Hash("leepadg");
            hash.Should().Be(680131659347);
        }

        [Test]
        public void Reverse_Hash_Should_Work()
        {
            var input = ReverseHash(680131659347);
            input.Should().Be("leepadg");
        }

        [Test]
        public void Solve_assignment()
        {
            Console.WriteLine(ReverseHash(910897038977002));
        }

        public static long Hash(String input)
        {
            long hash = 7;
            for(var i = 0; i < input.Length; i++)
            {
                hash = (hash*37 + Alphabet.IndexOf(input[i]));
            }
            return hash;
        }

        public static string ReverseHash(long hash)
        {
            var result = string.Empty;
            
            while (hash != 7)
            {
                var index = (int) (hash%37);
                hash = (hash - index)/37;

                result = Alphabet[index] + result;
            }

            return result;
        }
    }
}
